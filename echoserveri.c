#include "csapp.h"

#include <time.h>

void echo(int connfd);



char * get_current_time();
void print_current_time();



int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;



	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
//	while (1) {
//		clientlen = sizeof(clientaddr);
//		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
//
//		/* Determine the domain name and IP address of the client */
//		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
//					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
//		haddrp = inet_ntoa(clientaddr.sin_addr);
//		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
//
//		echo(connfd);
//		Close(connfd);
//	}


	// time.h
//	time_t t = time(NULL);
//	struct tm *tm = localtime(&t);
//	char string_time[64];
//	strftime(string_time, sizeof(string_time), "%c", tm);
	// time.h

	while (1)
	{
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);

		//printf("[%s]\t", string_time);
		printf("\n");
		print_current_time();
		printf("server connected to %s (%s).\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}

	exit(0);
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;



	char bufcpy[MAXLINE];



	int fd;
	char filename[MAXLINE];
	int flags = O_RDONLY;
	mode_t mode = 0;

	char buffer[MAXLINE];
	size_t nn = 0;

	char c;

	char buffer_size[MAXLINE];




	Rio_readinitb(&rio, connfd);
//	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
//		printf("server received %lu bytes\n", n);
//		Rio_writen(connfd, buf, n);
//	}

	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {

		strcpy(bufcpy,buf);
		bufcpy[strcspn(bufcpy, "\n")] = 0;

		print_current_time();
		printf("server '%s' filename (%lu bytes).\n", bufcpy, n);



		strcpy(filename,buf);
		filename[strcspn(filename, "\n")] = 0;

		fd = open(filename, flags, mode);
		if(fd == -1)
		{
			print_current_time();
			printf("error accessing '%s'\n", filename);


			////
			Rio_writen(connfd, "-1\n", strlen("-1\n"));
		}
		else
		{
			print_current_time();
			printf("file found\n");

			while(read(fd, &c, 1) != 0)
			{
				buffer[nn] = c;
				nn++;
			}



			close(fd);

//			switch(nn)
//			{
//				case BUFSIZ:
//					n = n-2;
//					break;
//				case BUFSIZ-1:
//					n = n-1;
//					break;
//
//			}
//			buffer[nn] = '\n';
//			nn++;
//			buffer[nn] = 0;

			if(nn == MAXLINE)
			{
				nn--;
			}
			buffer[nn] = '\n';
			nn++;


			////
			sprintf(buffer_size, "%lu\n", nn);
			////
			Rio_writen(connfd, buffer_size, strlen(buffer_size));



	//		printf("texto a copiar:\n");
	//		printf("%s", buffer);
	//		printf("...\n");

			//strcpy(buf,buffer);

	//		Rio_writen(connfd, buf, nn);
			Rio_writen(connfd, buffer, nn);

			print_current_time();
			printf("file content sended.\n");

		}
	}
}



char *get_current_time()
{
	time_t current_time;
	char *current_time_str;
	current_time_str = NULL;

	current_time = time(NULL);

	if (current_time == ((time_t)-1))
	{
		// error at get date/time
		//return 1;
		return NULL;
	}

	current_time_str = ctime(&current_time);

	if (current_time_str == NULL)
	{
        // error at convert date/time
		//return 2;
		return NULL;
	}

	//strtok(current_time_str, "\n");
	current_time_str[strcspn(current_time_str, "\n")] = 0;

	return current_time_str;
}

void print_current_time()
{
	char *current_time_str;
	current_time_str = NULL;

	current_time_str = get_current_time();
	if(current_time_str != NULL)
	{
		printf("[%s] ", current_time_str);
	}
}
