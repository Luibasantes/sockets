# Práctica: Sockets #

Aplicación cliente - servidor que permita la transferencia de archivos entre dos computadoras.
Fork de  [https://bitbucket.org/fexadom/ejemplo_sockets/](https://bitbucket.org/fexadom/ejemplo_sockets/), basado en los ejemplos del capítulo de libro [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/3e/home.html)


### Integrantes ###

* Luigi Basantes Zambrano
* Carlos Andrés Pogo Ubilla


### Compilación ###

* Compilar el servidor:
	* make server

* Compilar el cliente: 
	* make client

* Compilar todo:
	* make


### Uso ###
Ejecutar el servidor:

```
./server <port>
```
Ejemplo:

```
./server 8080
```

Ejecutar el cliente:

```
./client <host> <port> <filename>
```
Ejemplo:

```
./client 127.0.0.1 8080 testfile
```

o

```
./client <host> <port> <filename> <new filename>
```
Ejemplo:

```
./client 127.0.0.1 8080 testfile testfile_client
```
