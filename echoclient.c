#include "csapp.h"

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;



	char filename[MAXLINE];
	//int filename_len;
	char filenameserver[MAXLINE];
	int filenameserver_len;

	int fd;
	char filename_new[MAXLINE];
	int flags = O_WRONLY|O_CREAT|O_TRUNC;
	mode_t mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH;

	char buffer_size[MAXLINE];
	int writting = 0;



//	if (argc != 3) {
//		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
//		exit(0);
//	}
//	host = argv[1];
//	port = argv[2];


//	En esta seccion manejamos la parte que muestra un mensaje al usuario al querer correr la 	aplicacion con argumentos diferentes a los requeridos.
	if (argc < 4 || argc > 5) {
		fprintf(stderr, "usage: %s <host> <port> <filename> <new filename>\n", argv[0]);
		exit(0);
	}


//	En esta seccion almacenamos los datos recogidos como parametros al ejecutar el cliente que 		serian el Host, el puerto y el nombre del archivo
	host = argv[1];
	port = argv[2];
//	Manejamos una copia de los parametros recibidos
	strcpy(filename,argv[3]);
	strcpy(filenameserver,argv[3]);



	printf("\topening %s:%s connection.\n", host, port);



	clientfd = open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);



	printf("\taccess allowed.\n");



//	while (Fgets(buf, MAXLINE, stdin) != NULL) {
//		Rio_writen(clientfd, buf, strlen(buf));
//		Rio_readlineb(&rio, buf, MAXLINE);
//		Fputs(buf, stdout);
//	}

	//strcpy(filenameserver,filename);
	filenameserver_len = strlen(filenameserver);
	filenameserver[filenameserver_len] = '\n';
	filenameserver[filenameserver_len+1] = '\0';
	Rio_writen(clientfd, filenameserver, strlen(filenameserver));
//	filename_len = strlen(filename);
//	filename[filename_len] = '\n';
//	filename[filename_len+1] = '\0';
//	Rio_writen(clientfd, filename, strlen(filename));

	//sleep(5);


	//HANDSHAKE
	Rio_readlineb(&rio, buffer_size, MAXLINE);
	if(strcasecmp(buffer_size,"-1\n") == 0)
	{
		printf("\terror: '%s' file not found at %s:%s\n", filename, host, port);

		close(clientfd);
		exit(-1);
	}
	else
	{
		buffer_size[strcspn(buffer_size, "\n")] = 0;
		printf("\treading '%s' file (%s bytes) from %s:%s\n", filename, buffer_size, host, port);
	}



	Rio_readlineb(&rio, buf, MAXLINE);
//	Fputs(buf, stdout);




//	Manejamos un campo adiccional en caso de que el usuario quiera cambiar de nombre al 		archivo.
	if(argc == 5)
	{
		strcpy(filename_new,argv[4]);
	}
	else
	{
		strcpy(filename_new,argv[3]);
	}
	fd = open(filename_new, flags, mode);
//	Manejamos el error de -1 en caso que al abrir o crear el nuevo archivo se genere un 		problema de apertura. 
    	if(fd == -1)
    	{
		printf("\terror accessing '%s'\n", filename_new);
    	}
    	else
    	{
//	Caso contrario se accede a tratar de escribir en el archivo.
		writting = write(fd, buf, strlen(buf));

		if(writting == -1)
		{
			printf("\terror writing '%s'\n", filename_new);
		}
		else
		{
			printf("\twriting '%s' file (%s bytes) from %s:%s\n", filename_new, 		buffer_size, host, port);
		}

        close(fd);
    	}



	printf("\tclosing %s:%s connection.\n", host, port);



	close(clientfd);
	exit(0);
}
